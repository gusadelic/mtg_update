#!/bin/php-cli

<?php

$rootdir = $_SERVER['HOME'] . "/mtg/";

require($rootdir . 'mtg_update/price/price_functions.inc.php');

require($rootdir . 'mtg_update/mtg_functions.inc.php');
require($rootdir . 'mtg_update/mtg_global.inc.php');

$prices = array();

if ( $argc > 1 ) {
	$sets = array();
	foreach ( $argv as $index => $arg ) {
		if ( $index == 0 ) continue; 
		$sets[] = $arg;
	
		echo "Clearing old Prices for " . $arg . " ...  ";
		if ( clearPrices($db, $arg) ) { echo "Completed!\n"; }
		else die("Failed!\n " . $clear_prices->errorInfo()[2]);
	}
}
else {
	$sets =  getMTGPriceSets();
	echo "Clearing old Prices...  ";
	if ( clearPrices($db) ) { echo "Completed!\n";}
	else die("Failed!\n " . $clear_prices->errorInfo()[2]);
}



foreach ( $sets as $set ) {
	
	//echo ("Getting Prices for " . $set . "...\n");

	$sql = "INSERT INTO `prices` (`card_id`, `foil`, `fair_price`, `absoluteChangeSinceYesterday`, `absoluteChangeSinceOneWeekAgo`, `bestVendorBuylistPrice`, `lowest_price` ) VALUES \n";

	$card_data = json_decode(findPrices(str_replace(" ", "_", $set)), true);
	
	// Remove Duplicates
	$cards = array();
	$exists = array();
	foreach( $card_data as $element ) {
   		if( !in_array( $element['name'], $exists )) {
        	$cards[] = $element;
        	$exists[]    = $element['name'];
        }
    }

	$myset = convertSetName($set);
	$setcode = getSetCode( $myset, $db );

	$sql_array = array();

	//echo "Processing " . $set . "...\r";

	foreach ( $cards as $card ) {

		// Skip card names with (<digit>) in them other than 0 or 1.
		if ( preg_match("/ \([2-9]\)/", $card['name']) ) { continue; }

		// Get an array of image names for this card.  Covers variations.
		$search_names = getImageName($card['name'], $setcode, $db);

		// Skip card names that don't match in the database, print error.
		if ( !$search_names ) { error_log("Card Match Fail: " . $card['name'] . " - " . $setcode); continue;}

		foreach ($search_names as $search_name) {

			$id = getCardID( $search_name , $setcode, $db);
			//echo $id . ": " . $search_name . "\n";
			if ( !isset($id) )	echo "\nCard: - " . $name;

			$foil = "false";
			if ( isset($card['isFoil']) && $card['isFoil'] ) { $foil = "true"; } 

			$fair_price = 0.0;
			if ( isset($card['fair_price']) ) { $fair_price = $card['fair_price']; }

			$absoluteChangeSinceYesterday = 0;
			if ( isset($card['absoluteChangeSinceYesterday']) ) { $absoluteChangeSinceYesterday = $card['absoluteChangeSinceYesterday']; }

			$absoluteChangeSinceOneWeekAgo = 0;
			if ( isset($card['absoluteChangeSinceOneWeekAgo']) ) { $absoluteChangeSinceOneWeekAgo = $card['absoluteChangeSinceOneWeekAgo']; }

			$bestVendorBuylistPrice = 0;
			if ( isset($card['bestVendorBuylistPrice']) ) { $bestVendorBuylistPrice = $card['bestVendorBuylistPrice']; }

			$lowestPrice = 0;
			if ( isset($card->lowestPrice) ) { $lowestPrice = $card->lowestPrice; }
				
			$sql_array[$id] = "(" . $id . "," . $foil . "," . $fair_price . "," . $absoluteChangeSinceYesterday . "," . $absoluteChangeSinceOneWeekAgo . "," . $bestVendorBuylistPrice . "," . $lowestPrice . ")";
		}
	}


	foreach ($sql_array as $item ) {
		$sql .= $item . ",\n";
	}


	$sql = substr_replace($sql, ";", -2);

	echo "Adding " . $set . " to the Database...  ";


	$insert_prices = $db->prepare($sql);


	if ( $insert_prices->execute() ) {
		echo " Completed!\n";
	}
	else {
		echo "Update Failed.\n";
		$errorinfo = $insert_prices->errorInfo();
		echo ($errorinfo[2] . "\n");
	}

	$insert_prices = "";	


}


	echo "\n";
	echo "Memory Usage: " . convert(memory_get_usage(true)) . "\n";
	echo "Exec Time: " . $time = microtime(true) - $_SERVER["REQUEST_TIME_FLOAT"] . "\n";
	echo "\n";

$timestamp = $db->prepare("
	UPDATE `metadata` SET `pricesUpdated`= NOW()
");

if ( $timestamp->execute() ) {
	echo "All prices updated!\n";
}
else {
	echo "Prices updated but Timestamp failed!!\n";
}

?>

