<?php
function convert($size)
{
    $unit=array('b','kb','mb','gb','tb','pb');
    return @round($size/pow(1024,($i=floor(log($size,1024)))),2).' '.$unit[$i];
}


function findPrices($set) {
	$card = "";
	$url = "http://www.mtgprice.com/spoiler_lists/" . $set;	
	$html = file_get_contents_curl($url);

	//parsing begins here:
	$doc = new DOMDocument();
	libxml_use_internal_errors(true);
	if ( !$doc->loadHTML('<?xml encoding="UTF-8">' . $html) ) {
		die("Failed to create DOMDocument!\n");
	}
	$body = $doc->getElementsByTagName('body'); 
	$data = findData($body->item(0)->nodeValue);
	return $data;
}


function getPrices($set) {
	$card = "";
	$url = "http://www.mtgprice.com/spoiler_lists/" . $set;	
	$html = file_get_contents_curl($url);

	//parsing begins here:
	$doc = new DOMDocument();
	libxml_use_internal_errors(true);
	if ( !$doc->loadHTML('<?xml encoding="UTF-8">' . $html) ) {
		die("Failed to create DOMDocument!\n");
	}
	$body = $doc->getElementsByTagName('body'); 
	$scripts = $body->item(0)->getElementsByTagName('script');
	$lines = explode("\n", $scripts->item(2)->nodeValue);
	$results = substr($lines[2], 26, -2);
	return $results;
}

function getPrice($set, $name) {
	$description = "";
	$html = file_get_contents_curl("http://www.mtgprice.com/sets/" . $set . "/" . $name);

	//parsing begins here:
	$doc = new DOMDocument();
	@$doc->loadHTML($html);
	$nodes = $doc->getElementsByTagName('title');

	//get and display what you need:
	$title = $nodes->item(0)->nodeValue;

	$metas = $doc->getElementsByTagName('meta');

	for ($i = 0; $i < $metas->length; $i++)
	{
	    $meta = $metas->item($i);
	    if($meta->getAttribute('name') == 'description')
	        $description = $meta->getAttribute('content');
	}

	$price = "";

	foreach ( str_split($description) as $char ) {

		if ( strcmp( ')', $char ) == 0 && is_numeric($price) ) {
			echo $price;
			break;
		}
		else if ( strcmp( '$', $char ) == 0 ) {
			$price = "";
		}
		else {
			$price .= $char;
		}
	}

}

function findData($string) {
	
	$matches = array();
	//$pattern = "/\[({\"[a-zA-Z0-9\ \-\_\.\,]*\":\"?[a-zA-Z0-9\-\_\.\/\\\:\,\ ]*\"?,?},?)+\]/";
	$pattern = "/(\[.+\])/";
	preg_match_all($pattern, $string, $matches);

	return $matches[1][1];
	
}

function getMTGPriceSets() {
        $html = file_get_contents_curl("http://www.mtgprice.com/magic-the-gathering-prices.jsp");

        //parsing begins here:
        $doc = new DOMDocument();
        @$doc->loadHTML($html);

        $tbody = $doc->getElementsByTagName('tbody');
	$a = $tbody->item(1)->getElementsByTagName('a');

	$items = array();
	
	for ($i = 0; $i < $a->length; $i++) {
        	$items[] = $a->item($i)->nodeValue;
	}

        return $items;

}

function convertSetName($setname) {
	$mysetname = "";

	$setname = str_replace(" (Foil)", "", $setname);

	// Annual Core Sets
	if ( preg_match("/M\d/", $setname) ) {
		$mysetname = str_replace("M", "Magic 20", $setname);
		if ( preg_match("/14/", $setname) || preg_match("/15/", $setname) ) {
			$mysetname .= " Core Set";
		}
		
	}
	// 6th Edition
	else if ( strcmp("6th Edition", $setname) == 0 ){
		$mysetname = "Classic Sixth Edition";
	}
	// 15th Anniversary
	else if ( strcmp("15th_Anniversary", $setname) == 0 ){
		$mysetname = "15th Anniversary";
	}
	//4th - 10th Edition
	else if ( preg_match("/\dth+/", $setname) ) {
		switch ($setname) {
			case "4th Edition": $mysetname = "Fourth Edition";
					break;
			case "5th Edition": $mysetname = "Fifth Edition";
					break;
			case "7th Edition": $mysetname = "Seventh Edition";
					break;
			case "8th Edition": $mysetname = "Eighth Edition";
					break;
			case "9th Edition": $mysetname = "Ninth Edition";
					break;
			case "10th Edition": $mysetname = "Tenth Edition";
					break;
		}
	}
	// Alpha or Beta
	else if ( preg_match("/Alpha/", $setname) || preg_match("/Beta/", $setname) ) {
		$mysetname = "Limited Edition " . $setname;
	}
	// Unlimited or Revised
	else if ( preg_match("/Revised/", $setname) || preg_match("/Unlimited/", $setname) ) {
		$mysetname = $setname . " Edition";
	}
	// Dual Decks
	else if ( preg_match("/Duel Decks /", $setname) ) {
		$mysetname = str_replace("Decks", "Decks:" , $setname);
		$mysetname = str_replace("vs", "vs.", $mysetname);
	}
	// From the Vault
	else if ( preg_match("/From the Vault /", $setname) ) {
		if ( preg_match("/Annihilation/", $setname) ) {
			$mysetname = "From the Vault: Annihilation (2014)";
		}
		else {
			$mysetname = str_replace("Vault", "Vault:", $setname);
		}
	}
	// Premium Decks
	else if ( preg_match("/Premium Deck /", $setname) ) {
		$mysetname = str_replace("Series", "Series:", $setname);
	}
	// Commander
	else if ( preg_match("/Commander/", $setname) ) {
		switch ($setname) {
			case "Commanders Arsenal": $mysetname = "Commander's Arsenal";
					break;

			case "Commander": $mysetname = "Magic: The Gathering-Commander";
					break;

			case "Commander 2013": $mysetname = $setname . " Edition";
					break;

			default: $mysetname = $setname; 
		}
	}
	// Urza's Sets
	else if ( preg_match("/Urzas /", $setname) ) {
		$mysetname = str_replace("Urzas", "Urza's", $setname);
	}
	// Planechase & Planechase Planes
	else if ( preg_match("/Planechase/", $setname) ) {
		if ( preg_match("/2012/", $setname) ) {
			$mysetname = "Planechase 2012 Edition";
		}
		else {
			$mysetname = "Planechase";
		}
	}
	// Archenemy and Archenemy Schemes
	else if ( preg_match("/Archenemy/", $setname) ) {
		$mysetname = "Archenemy";
	}
	// Conspiracy and Conspiracy Schemes
	else if ( preg_match("/Conspiracy/", $setname) ) {
		$mysetname = "Magic: The Gathering—Conspiracy";
	}
	// Miscelaneous
	else {
		switch ($setname) {
			case "Dragons Maze": $mysetname = "Dragon's Maze";
					break;

			case "Euro Land Program": $mysetname = "European Land Program";
					break;

			case "Game Day": $mysetname = "Magic Game Day";
					break;
		
			case "Player Rewards": $mysetname = "Magic Player Rewards";
					break;
		
			case "Ravnica": $mysetname = "Ravnica: City of Guilds";
					break;

			case "Champs": $mysetname = "Champs and States";
					break;

			case "Gateway": $mysetname = "WPN and Gateway";
					break;

			case "Timespiral Timeshifted": $mysetname = 'Time Spiral "Timeshifted"';
					break;

			case "Two-Headed Giant": $mysetname = "Two-Headed Giant Tournament";
					break;

			case "Deckmasters Box Set": $mysetname = "Deckmasters";
					break;

			case "World Magic Cup Qualifier": $mysetname = "World Magic Cup Qualifiers";
					break;

			case "Modern Masters 2015": $mysetname = "Modern Masters 2015 Edition";
					break;

			default: $mysetname = $setname;
		}
	}
	

	return $mysetname;		
}

function clearPrices($db, $set = NULL) {
	if ( !isset($set) ) {
		$clear_prices = $db->prepare("TRUNCATE TABLE  `prices`");
		return $clear_prices->execute();
	}
	else {
		$params = array( ':set' => $set );
		$clear_prices = $db->prepare("DELETE FROM prices USING prices INNER JOIN (SELECT id FROM cards WHERE cards.set = :set) c ON prices.card_id = c.id");
		return $clear_prices->execute($params);
	}

}



?>
