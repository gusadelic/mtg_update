<?php
$rootdir = $_SERVER['HOME'] . "/mtg/";
$homedir = $rootdir . "mtg_update/";
//$source_location = "/data/mtg/mtgjson/web/json/";
$source_location = "http://mtgjson.com/";

require($homedir . 'mtg_global.inc.php');
require($homedir . 'mtg_functions.inc.php');
require('database_functions.inc.php');



if ( $argc > 1 ) {
	if ( strcmp($argv[1], "freshinstall") == 0 ) {
		clearDatabase($db);
		exec("rm -f " . $homedir . "database/json/*");
	} 
}

if ( !file_exists($homedir . "database/json/version-full.json") ) { $my_version = array('version' => "0");}
else {
	$my_version = json_decode( file_get_contents($homedir . "database/json/version-full.json"), TRUE);
}

$current_version = json_decode( file_get_contents($source_location . "json/version-full.json"), TRUE);

if ( strcmp($my_version['version'], $current_version['version']) == 0) {

	echo "Database is up to date!\n";

}
else if ( $my_version['version'] == "0" ) {
	echo "Performing a Fresh Install of the Database.\n";
	echo "Current Version: " . $current_version['version'] . "\n\n";

	installNewSets($source_location, $homedir, $db);
	//updateVersion($source_location, $homedir);
	updateVersion($source_location, $homedir, $db);

	$timestamp = $db->prepare("
		UPDATE `metadata` SET `cardsUpdated` = NOW(), `cardsVersion` = :version
	");

	$timestamp->execute(array(':version' => $current_version['version']));

}
else {

	echo "Database is out of date!  Your version is " . $my_version['version'] . ".\n";
	echo "Current Version: " . $current_version['version'] . "\n\n";

	clearDatabase($db);
		
	exec("rm -f " . $homedir . "database/json/*");

	installNewSets($source_location, $homedir, $db);

	//updateVersion($source_location, $homedir);
	updateVersion($source_location, $homedir, $db);


	$timestamp = $db->prepare("
		UPDATE `metadata` SET `cardsUpdated` = NOW(), `cardsVersion` = :version
	");

	$timestamp->execute(array(':version' => $current_version['version']));

}





?>
