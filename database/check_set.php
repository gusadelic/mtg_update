<?php
$set = "";
$homedir = "/data/mtg/database/";

if ( $argc > 1 ) {
	$set = $argv[1];
}
else { die("Set code required'\n"); }

$check_set = json_decode( file_get_contents( $homedir . "json/" . $set . "-x.json"), TRUE);

foreach ( $check_set['cards'] as $card ) {
	if ( isset($card['legalities']) ) {
		var_dump($card['legalities']);
	}
}

?>
