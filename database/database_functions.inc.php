<?php


function getName($code, $source) {
	$set_names = json_decode( file_get_contents($source . "json/SetList.json"), TRUE);
	foreach ( $set_names as $set ) {
		if ( strcmp($set['code'], $code) == 0 ) return $set['name'];
	}


}

function downloadFile ($url, $path) {

  $newfname = $path;
  $file = fopen ($url, "rb");
  if ($file) {
    $newf = fopen ($newfname, "wb");

    if ($newf) {
	    while(!feof($file)) {
	      fwrite($newf, fread($file, 1024 * 8 ), 1024 * 8 );
	    }
	    echo ( "Download complete! ");
	}
    else {
    	echo ( "Failed to write!");
    }
  }

  if ($file) {
    fclose($file);
  }

  if ($newf) {
    fclose($newf);
  }
 }


function updateExistingSets($source, $home, $db) {
	$my_sets = json_decode( file_get_contents($home . "database/json/SetCodes.json"), TRUE);

	$updates = array();

	foreach ( $my_sets as $set ) {

		$source_modified =  date ("F d Y H:i:s.", filemtime_remote($source . "json/" . $set . "-x.json") );
		$home_modified = date ("F d Y H:i:s.", filemtime($home . "database/json/" . $set . "-x.json") );


		if ( strcmp($source_modified, $home_modified) > 0  ) {
			$updates[] = $set;
		}

	}

	if ( count($updates) > 0 ) {
		echo "The following sets will be updated: \n";
		foreach( $updates as $set ) {
			echo $set . " - " . getName($set, $source) . " - ";
			downloadFile($source . "json/" . $set . "-x.json", $home . "database/json/" . $set . "-x.json");
			$update_set = json_decode( file_get_contents( $home . "database/json/" . $set . "-x.json"), TRUE);
			updateSet($update_set, $db);
			echo "\n";
		}
		
	}
	else {
		echo "There are no updates at this time.\n";
	}


	echo "\n";
}

function updateCardColors($card_id, $colors, $db) {
	$params = array(
		':id' => $card_id,
	);
	$clear_colors = $db->prepare('
		DELETE FROM `card_colors` WHERE `card_id` = :id
	');
	$clear_colors->execute($params);

	addCardColors($card_id, $colors, $db);
}

function updateCardLegalities($card_id, $legalities, $db) {
	$sql = "";

	foreach ( $legalities as $l_key => $format ) {
		if ( $l_key == "" || $format == "" ) { continue; }		

		$this_format = $l_key;

		$sql .= "`" . $this_format . "`='" . $format . "',";

		$add_format_query = $db->prepare("
			ALTER TABLE card_legality ADD `" . $this_format . "` VARCHAR(16)
		");

		$add_format_query->execute();
		
	}

	$sql = substr($sql, 0, -1);

	$add_legality_query = $db->prepare("
		UPDATE `card_legality` SET " . $sql . " WHERE `card_id` = " . $card_id );

	if ( !$add_legality_query->execute() ) {
		$errorinfo = $add_legality_query->errorInfo();
		die("Legality Update Failed!\n". $sql . "\n" . $errorinfo[2] . "\n");
	}
}


function updateCardNames($card_id, $names, $db) {
	$params = array(
		':id' => $card_id,
	);
	$clear_names = $db->prepare('
		DELETE FROM `card_names` WHERE `card_id` = :id
	');
	$clear_names->execute($params);

	addCardNames($card_id, $names, $db);
}

function updateCardPrintings($card_id, $printings, $db) {
	$params = array(
		':id' => $card_id,
	);
	$clear_printings = $db->prepare('
		DELETE FROM `card_printings` WHERE `card_id` = :id
	');
	$clear_printings->execute($params);

	addCardPrintings($card_id, $printings, $db);
}

function updateCardRulings($card_id, $rulings, $db) {
	$params = array(
		':id' => $card_id,
	);
	$clear_ruleings = $db->prepare('
		DELETE FROM `card_rulings` WHERE `card_id` = :id
	');
	$clear_ruleings->execute($params);

	addCardRulings($card_id, $rulings, $db);
}

function updateCardSubtypes($card_id, $subtypes, $db) {
	$params = array(
		':id' => $card_id,
	);
	$clear_subtypes = $db->prepare('
		DELETE FROM `card_subtypes` WHERE `card_id` = :id
	');
	$clear_subtypes->execute($params);				

	addCardSubtypes($card_id, $subtypes, $db);
}

function updateCardTypes($card_id, $types, $db) {
	$params = array(
		':id' => $card_id,
	);
	$clear_types = $db->prepare('
		DELETE FROM `card_types` WHERE `card_id` = :id
	');
	$clear_types->execute($params);

	addCardTypes($card_id, $types, $db);
}

function updateCardVariations($card_id, $variations, $db) {
	$params = array(
		':card_id' => $card_id,
	);
	$clear_variations = $db->prepare('
		DELETE FROM `card_variations` WHERE `card_id` = :card_id
	');
	$clear_variations->execute($params);

	addCardVariations($card_id, $variations, $db);
}

function updateCard($card_id, $card, $set_code, $db) {
	if ( !isset($card['power']) ) { $card['power'] = ""; }
	if ( !isset($card['toughness']) ) { $card['toughness'] = ""; }
	if ( !isset($card['flavor']) ) { $card['flavor'] = ""; }
	if ( !isset($card['layout']) ) { $card['layout'] = ""; }
	if ( !isset($card['loyalty']) ) { $card['loyalty'] = ""; }
	if ( !isset($card['multiverseid']) ) { $card['multiverseid'] = ""; }
	if ( !isset($card['watermark']) ) { $card['watermark'] = ""; }
	if ( !isset($card['hand']) ) { $card['hand'] = ""; }
	if ( !isset($card['life']) ) { $card['life'] = ""; }
	if ( !isset($card['originalType']) ) { $card['originalType'] = ""; }
	if ( !isset($card['originalText']) ) { $card['originalText'] = ""; }
	if ( !isset($card['number']) ) { $card['number'] = "0";}
	if ( !isset($card['cmc']) ) { $card['cmc'] = "";}
	if ( !isset($card['manaCost']) ) { $card['manaCost'] = "";}
	if ( !isset($card['text']) ) { $card['text'] = "";}

	$params = array(
		':id' => $card_id,
		':set' => $set_code,	
		':layout' => $card['layout'],
		':name' => $card['name'],
		':manaCost' => $card['manaCost'],
		':cmc' => $card['cmc'],
		':type' => $card['type'],
		':rarity' => $card['rarity'],
		':text' => $card['text'],
		':flavor' => $card['flavor'],
		':artist' => $card['artist'],
		':number' => $card['number'],
		':power' => $card['power'],
		':toughness' => $card['toughness'],
		':loyalty' => $card['loyalty'],
		':multiverseid' => $card['multiverseid'],
		':imageName' => $card['imageName'],
		':watermark' => $card['watermark'],
		':hand' => $card['hand'],
		':life' => $card['life'],
		':originalType' => $card['originalType'],
		':originalText' => $card['originalText']
	);	

	$add_card_query = $db->prepare("
		UPDATE `cards` 
		SET `layout`=:layout, `name`=:name, `manaCost`=:manaCost, `cmc`=:cmc, `type`=:type, `rarity`=:rarity, `text`=:text, `flavor`=:flavor, `artist`=:artist, `number`=:number, `power`=:power, `toughness`=:toughness, `loyalty`=:loyalty, `multiverseid`=:multiverseid, `imageName`=:imageName, `watermark`=:watermark, `hand`=:hand, `life`=:life, `originalType`=:originalType, `originalText`=:originalText, `set`=:set 
		WHERE `id`=:id
		");

	if ( !$add_card_query->execute($params) ) {
		$errorinfo = $add_card_query->errorInfo();
		die("Card Import Failed!\n" . $errorinfo[2] . "\n");
	}
}

function updateSet($set, $db) {
	echo "Updating... ";
	$set_code = $set['code'];

	// Add the cards!
		foreach ( $set['cards'] as $key => $card ) {
			$card_id = getCardIDFromName($card['name'], $set_code, $db);
			if ( !$card_id ) { die($card['name'] . " - " . $set_code . "\n");}
			
			updateCard($card_id, $card, $set_code, $db);

			if ( isset($card['colors']) ) updateCardColors($card_id, $card['colors'], $db);
			
			if ( isset($card['legalities']) ) updateCardLegalities($card_id, $card['legalities'], $db);

			if ( isset($card['names']) ) updateCardNames($card_id, $card['names'], $db);

			if ( isset($card['printings']) ) updateCardPrintings($card_id, $card['printings'], $db);

			if ( isset($card['rulings']) ) updateCardRulings($card_id, $card['rulings'], $db);

			if ( isset($card['subtypes']) ) updateCardSubtypes($card_id, $card['subtypes'], $db);

			/*
			if ( isset($card['supertypes']) )
			foreach ( $card['supertypes'] as $supertype) {
				$params = array(
					':multiverseid' => $card['multiverseid'],
					':supertype' => $supertype
				);

				$add_supertypes_query = $db->prepare("
					INSERT INTO `card_subtypes`(`multiverseid`, `supertype`) 
	 				VALUES (:multiverseid,:supertype)
	 			");
				if ( !$add_supertypes_query->execute($params) ) {
					die("Supertypes Import Failed!");
				}
			}
			*/

			if ( isset($card['types']) ) updateCardTypes($card_id, $card['types'], $db);

			if ( isset($card['variations']) ) updateCardVariations($card_id, $card['variations'], $db);
		
		}

		echo "Set Updated! ";
 }



 	

function installNewSets($source, $home, $db) {

		setUpdating($db, 1);

		$match = false;
		$updates = array();

		if ( !file_exists($home . "database/json/SetCodes.json") ) { $my_sets = array(); }
		else {
			$my_sets = json_decode( file_get_contents($home . "database/json/SetCodes.json"), TRUE);
		}
		$current_sets = json_decode( file_get_contents($source . "json/SetCodes.json"), TRUE);

		if ( !$my_sets ) { $my_sets = array(); }
	
		foreach ( $current_sets as $set ) {
			foreach ( $my_sets as $my_set ) {
				$compare = strcmp(strtoupper($set), strtoupper($my_set) );
				if ( $compare == 0 ) {
					$match = true;
					break;
				}
			}

			if ( !$match ) {

				$updates[] = $set;
			}

			$match = false;
		}

		if ( count($updates) > 0 ) {
			echo "The following sets will be installed: \n";
			foreach( $updates as $set ) {
					echo $set . " - " . getName($set, $source) . " - ";
					downloadFile($source . "json/" . $set . "-x.json", $home . "database/json/" . $set . "-x.json");
					$add_set = json_decode( file_get_contents( $home . "database/json/" . $set . "-x.json"), TRUE);
					addSet($add_set, $db);
					echo "\n";
			}	
		}
		else {
			echo "There are no new sets to install.\n";
		}

	echo "\n";

	setUpdating($db, 0);

}


function addCard($card, $set_code, $db) {

	if ( !isset($card['power']) ) { $card['power'] = ""; }
	if ( !isset($card['toughness']) ) { $card['toughness'] = ""; }
	if ( !isset($card['flavor']) ) { $card['flavor'] = ""; }
	if ( !isset($card['layout']) ) { $card['layout'] = ""; }
	if ( !isset($card['loyalty']) ) { $card['loyalty'] = ""; }
	if ( !isset($card['multiverseid']) ) { $card['multiverseid'] = ""; }
	if ( !isset($card['watermark']) ) { $card['watermark'] = ""; }
	if ( !isset($card['hand']) ) { $card['hand'] = ""; }
	if ( !isset($card['life']) ) { $card['life'] = ""; }
	if ( !isset($card['originalType']) ) { $card['originalType'] = ""; }
	if ( !isset($card['originalText']) ) { $card['originalText'] = ""; }
	if ( !isset($card['number']) ) { $card['number'] = "0";}
	if ( !isset($card['cmc']) ) { $card['cmc'] = "";}
	if ( !isset($card['manaCost']) ) { $card['manaCost'] = "";}
	if ( !isset($card['text']) ) { $card['text'] = "";}

	$params = array(
		':set' => $set_code,	
		':layout' => $card['layout'],
		':name' => $card['name'],
		':manaCost' => $card['manaCost'],
		':cmc' => $card['cmc'],
		':type' => $card['type'],
		':rarity' => $card['rarity'],
		':text' => $card['text'],
		':flavor' => $card['flavor'],
		':artist' => $card['artist'],
		':number' => $card['number'],
		':power' => $card['power'],
		':toughness' => $card['toughness'],
		':loyalty' => $card['loyalty'],
		':multiverseid' => $card['multiverseid'],
		':imageName' => $card['imageName'],
		':watermark' => $card['watermark'],
		':hand' => $card['hand'],
		':life' => $card['life'],
		':originalType' => $card['originalType'],
		':originalText' => $card['originalText']
	);	

	$add_card_query = $db->prepare("
		INSERT INTO `cards`(`layout`, `name`, `manaCost`, `cmc`, `type`, `rarity`, `text`, `flavor`, `artist`, `number`, `power`, `toughness`, `loyalty`, `multiverseid`, `imageName`, `watermark`, `hand`, `life`, `originalType`, `originalText`, `set`) 
		VALUES (:layout,:name,:manaCost,:cmc,:type,:rarity,:text,:flavor,:artist,:number,:power,:toughness,:loyalty,:multiverseid,:imageName,:watermark,:hand,:life,:originalType,:originalText,:set)
	");

	if ( !$add_card_query->execute($params) ) {
		$errorinfo = $add_card_query->errorInfo();
		die("Card Import Failed!\n" . $errorinfo[2] . "\n");
	}

}


function addCardColors($card_id, $colors, $db) {
	foreach ( $colors as $color) {
		$params = array(
			':color' => $color,
			':card_id' => $card_id
		);

		$add_colors_query = $db->prepare("
			INSERT INTO `card_colors`(`card_id`,`color`) 
				VALUES (:card_id, :color)
			");
		if ( !$add_colors_query->execute($params) ) {
			$errorinfo = $add_colors_query->errorInfo();
			die("Colors Import Failed!\n" . $errorinfo[2] . "\n");
		}
	}
}

function addCardLegalities($card_id, $legalities, $db) {
	$sql_cols = "`card_id`,";
	$sql_vals = "'" . $card_id . "',";


	foreach ( $legalities as $l_key => $format) {

		$this_format = $l_key;

		$sql_cols .= "`" . $this_format . "`,";
		$sql_vals .= "'" . $format . "',";

		$add_format_query = $db->prepare("
			ALTER TABLE card_legality ADD `" . $this_format . "` VARCHAR(16)
		");

		$add_format_query->execute();

	}

	$sql_cols = substr($sql_cols, 0, -1);
	$sql_vals = substr($sql_vals, 0, -1);

	$add_legality_query = $db->prepare("
		INSERT INTO `card_legality`(" . $sql_cols . ") VALUES (" . $sql_vals . ")
		");
	
	if ( !$add_legality_query->execute() ) {
		$errorinfo = $add_legality_query->errorInfo();
		die("Legality Import Failed!\n". $sql_cols . "\n" . $sql_vals . "\n" . $errorinfo[2] . "\n ");
	}
}

function addCardNames($card_id, $names, $db) {
	foreach ( $names as $name) {
		$params = array(
			':card_id' => $card_id,
			':name' => $name
		);

		$add_names_query = $db->prepare("
			INSERT INTO `card_names`(`card_id`, `name`) 
			VALUES (:card_id,:name)
		");
		if ( !$add_names_query->execute($params) ) {
			$errorinfo = $add_names_query->errorInfo();
			die("Names Import Failed!\n" . $errorinfo[2] . "\n");
		}
	}
}

function addCardPrintings($card_id, $printings, $db) {
	foreach ( $printings as $set) {
		$params = array(
			':set' => $set,
			':card_id' => $card_id
		);

		$add_printings_query = $db->prepare("
			INSERT INTO `card_printings`(`card_id`, `set`) 
				VALUES (:card_id,:set)
			");
		if ( !$add_printings_query->execute($params) ) {
			$errorinfo = $add_printings_query->errorInfo();
			die("Printings Import Failed!\n" . $errorinfo[2] . "\n ");
		}
	}
}

function addCardRulings($card_id, $rulings, $db) {
	foreach ( $rulings as $ruling) {
		$params = array(
			':date' => $ruling['date'],
			':text' => $ruling['text'],
			':card_id' => $card_id
		);

		$add_rulings_query = $db->prepare("
			INSERT INTO `card_rulings`(`date`, `text`, `card_id`) 
				VALUES (:date,:text,:card_id)
			");
		if ( !$add_rulings_query->execute($params) ) {
			$errorinfo = $add_rulings_query->errorInfo();
			die("Rulings Import Failed!\n" . $errorinfo[2] . "\n");
		}
	}
}

function addCardSubtypes($card_id, $subtypes, $db) {
	foreach ( $subtypes as $subtype) {
		$params = array(
			':subtype' => $subtype,
			':card_id' => $card_id
		);

		$add_subtypes_query = $db->prepare("
			INSERT INTO `card_subtypes`(`subtype`, `card_id`) 
				VALUES (:subtype,:card_id)
			");
		if ( !$add_subtypes_query->execute($params) ) {
			$errorinfo = $add_subtypes_query->errorInfo();
			die("Subtypes Import Failed!\n" . $errorinfo[2] . "\n");
		}
	}
}

function addCardTypes($card_id, $types, $db) {
	foreach ( $types as $type) {
		$params = array(
			':type' => $type,
			':card_id' => $card_id
		);

		$add_types_query = $db->prepare("
			INSERT INTO `card_types`(`type`, `card_id`) 
				VALUES (:type,:card_id)
			");
		if ( !$add_types_query->execute($params) ) {
			$errorinfo = $add_types_query->errorInfo();
			die("Types Import Failed!\n" . $errorinfo[2] . "\n");
		}
	}
}

function addCardVariations($card_id, $variations, $db) {
	foreach ( $variations as $variation) {
		$params = array(
			':card_id' => $card_id,
			':variation' => $variation
		);

		$add_variations_query = $db->prepare("
			INSERT INTO `card_variations`(`card_id`, `variation`) 
				VALUES (:card_id,:variation)
			");
		if ( !$add_variations_query->execute($params) ) {
			$errorinfo = $add_variations_query->errorInfo();
			die("Variations Import Failed!\n" . "CardID: " . $card_id . " Variation: " . $variation . "\n" . $errorinfo[2] . "\n");
		}
	}
}


 function addSet($set, $db) {
 	echo "Installing... ";

 	$set_code = $set['code'];

 	if ( !isset($set['block']) ) { $set['block'] = "Promo"; }
 	if ( !isset($set['gathererCode']) ) { $set['gathererCode'] = ""; }
 	if ( !isset($set['oldCode']) ) { $set['oldCode'] = ""; }
 	if ( !isset($set['border']) ) { $set['border'] = "white"; } 

	$params = array(
		':code' => $set['code'],
		':gathererCode' => $set['gathererCode'],
		':oldCode' => $set['oldCode'],
		':name' => $set['name'],
		':releaseDate' => $set['releaseDate'],
		':border' => $set['border'],
		':type' => $set['type'],
		':block' => $set['block']
	);

	$add_set_query = $db->prepare("
		INSERT INTO `sets`(`code`, `gathererCode`, `oldCode`, `name`, `releaseDate`, `border`, `type`, `block`)
		VALUES (:code, :gathererCode, :oldCode, :name, :releaseDate, :border, :type, :block)
		");

	if ( !$add_set_query->execute($params) ) {
		$errorinfo = $add_set_query->errorInfo();
		die("Set Import Failed!\n " . $errorinfo[2] . "\n ");
	}
	else {

		// Add the cards!
		foreach ( $set['cards'] as $key => $card ) {
			
			addCard($card, $set_code, $db);
		}

		// Add all the extended card information
		foreach ( $set['cards'] as $key => $card ) {

			$card_id = getCardIDFromName($card['name'], $set_code, $db);

			if ( isset($card['colors']) ) addCardColors($card_id, $card['colors'], $db);
		
			if ( isset($card['legalities']) ) addCardLegalities($card_id, $card['legalities'], $db);

			if ( isset($card['names']) ) addCardNames($card_id, $card['names'], $db);

			if ( isset($card['printings']) ) addCardPrintings($card_id, $card['printings'], $db);

			if ( isset($card['rulings']) ) addCardRulings($card_id, $card['rulings'], $db);

			if ( isset($card['subtypes']) ) addCardSubtypes($card_id, $card['subtypes'], $db);


			/*
			if ( isset($card['supertypes']) )
			foreach ( $card['supertypes'] as $supertype) {
				$params = array(
					':multiverseid' => $card['multiverseid'],
					':supertype' => $supertype
				);

				$add_supertypes_query = $db->prepare("
					INSERT INTO `card_subtypes`(`multiverseid`, `supertype`) 
	 				VALUES (:multiverseid,:supertype)
	 			");
				if ( !$add_supertypes_query->execute($params) ) {
					die("Supertypes Import Failed!");
				}
			}
			*/

			if ( isset($card['types']) ) addCardTypes($card_id, $card['types'], $db);

			if ( isset($card['variations']) ) addCardVariations($card_id, $card['variations'], $db);

		}
		echo "Set Installed! ";
 	}

 }


function updateVersion($source, $home, $db) {

		echo "Updating Sets and Version: \n";
		echo "SetList.json - ";
		downloadFile($source . "json/SetList.json", $home . "database/json/SetList.json");
		echo "\n";
		echo "SetCodes.json - ";
		downloadFile($source . "json/SetCodes.json", $home . "database/json/SetCodes.json");
		echo "\n";	
		echo "version-full.json - ";
		downloadFile($source . "json/version-full.json", $home . "database/json/version-full.json");
		echo "\n";

		fixTimeshifted($db);
		fixOnline($db);
}


function filemtime_remote($uri)
{
    $uri = parse_url($uri);
    $handle = @fsockopen($uri['host'],80);
    if(!$handle)
        return 0;

    fputs($handle,"GET $uri[path] HTTP/1.1\r\nHost: $uri[host]\r\n\r\n");
    $result = 0;
    while(!feof($handle))
    {
        $line = fgets($handle,1024);
        if(!trim($line))
            break;

        $col = strpos($line,':');
        if($col !== false)
        {
            $header = trim(substr($line,0,$col));
            $value = trim(substr($line,$col+1));
            if(strtolower($header) == 'last-modified')
            {
                $result = strtotime($value);
                break;
            }
        }
    }
    fclose($handle);
    return $result;
}

function fixTimeshifted($db) {
	$timeshift = $db->prepare("
		UPDATE `sets` SET `type`='reprint', `block`='Promo'
		WHERE `code` = 'TSB'
	");

	$timeshift->execute();

}

function fixOnline($db) {
	$timeshift = $db->prepare("
		UPDATE `sets` SET `type`='online', `block`='Promo'
		WHERE `code` = 'TPR' OR `code` = 'VMA' OR `code` = 'ME4' OR `code` = 'ME3' OR `code` = 'ME2' OR `code` = 'ME1'
	");

	$timeshift->execute();

}


function clearDatabase($db) {
	$clear_database = $db->prepare("
		SET FOREIGN_KEY_CHECKS = 0;
		TRUNCATE TABLE `card_colors`;
		TRUNCATE TABLE `card_legality`;
		TRUNCATE TABLE `card_names`;
		TRUNCATE TABLE `card_printings`;
		TRUNCATE TABLE `card_rulings`;
		TRUNCATE TABLE `card_subtypes`;
		TRUNCATE TABLE `card_supertypes`;
		TRUNCATE TABLE `card_types`;
		TRUNCATE TABLE `card_variations`;
		TRUNCATE TABLE `cards`;
		TRUNCATE TABLE `sets`;
	");

	if (!$clear_database->execute() ) {
		$errorinfo = $clear_database->errorInfo();
		die("Failed to clear Database!\n" . $errorinfo[2] . "\n");
	}
	else {
		echo "Database cleared!\n";
	}

}

?>
