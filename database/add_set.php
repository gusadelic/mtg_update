<?php

require("/data/mtg/global.inc.php");
require('/data/mtg/functions.inc.php');
require('database_functions.inc.php');

$home = "/data/mtg/database/";
//$source_location = "/data/mtg/mtgjson/web/json/";
$source = "http://mtgjson.com/json/";

$set = "";

if ( $argc > 1 ) { $set = $argv['1']; }
else { die("Set Code required!\n"); }

echo $set . " - " . getName($set) . " - ";
downloadFile($source . $set . "-x.json", $home . "json/" . $set . "-x.json");
$add_set = json_decode( file_get_contents( $home . "json/" . $set . "-x.json"), TRUE);
addSet($add_set, $db);
echo "\n";


?>