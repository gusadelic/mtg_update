#!/bin/bash
rm /var/www/mtg/mtg.sql.5
mv /var/www/mtg/mtg.sql.4 /var/www/mtg/mtg.sql.5
mv /var/www/mtg/mtg.sql.3 /var/www/mtg/mtg.sql.4
mv /var/www/mtg/mtg.sql.2 /var/www/mtg/mtg.sql.3
mv /var/www/mtg/mtg.sql.1 /var/www/mtg/mtg.sql.2
mv /var/www/mtg/mtg.sql /var/www/mtg/mtg.sql.1

mysqldump -u root --password=gusadelic --ignore-table=mtg.condition --ignore-table=mtg.groups --ignore-table=mtg.language --ignore-table=mtg.users --ignore-table=mtg.stock mtg > /var/www/mtg/mtg.sql
