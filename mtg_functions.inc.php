<?php

function file_get_contents_curl($url)
{
    $ch = curl_init();

    curl_setopt($ch, CURLOPT_HEADER, 0);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);


        $limit  =   0;
        while((curl_errno($ch) == 28 || $limit == 0) && $limit < 5){
                if ( $limit > 0 ) {
                        echo "Connection Timed out.  Retrying... Attempt " . $limit . "\n";
                        sleep(1);
                }

                $limit++;
                $data = curl_exec($ch);
        }


    curl_close($ch);

    return $data;
}


function getCardID($card, $setID, $db) {

	$params = array(':name' => $card, ':set' => $setID);

	$get_id_query = $db->prepare('
		SELECT `id` FROM `cards` WHERE `imageName` = :name AND `set` = :set
	');

	$get_id_query->execute($params);

	$id = $get_id_query->fetch();

	return $id[0];
}


function getCardIDFromName($card, $setID, $db) {

	$params = array(':name' => $card, ':set' => $setID);

	$get_id_query = $db->prepare('
		SELECT `id` FROM `cards` WHERE `name` = :name AND `set` = :set
	');

	$get_id_query->execute($params);

	$id = $get_id_query->fetch();

	return $id[0];
}


function getSetName($code, $db) {

	$params = array( ':set' => $code );

	$get_name_query = $db->prepare('
		SELECT name FROM sets WHERE code = :set
	');

	$get_name_query->execute($params);

	$name = $get_name_query->fetch();

	return $name[0];
}


function getSetCode($setname, $db) {

	$params = array( ':setname' => $setname );

	$get_code_query = $db->prepare('
		SELECT code FROM sets WHERE name = :setname
	');

	$get_code_query->execute($params);

	$code = $get_code_query->fetch();

	return $code[0];
}

function getSetCodes($db){
	$get_sets = $db->prepare("
		SELECT code FROM sets
	");

	$get_sets->execute();

	$sets = $get_sets->fetchAll(PDO::FETCH_OBJ);

	return $sets;
}

function getImageName($name, $set, $db) {

	$name = preg_replace('/Ae(?=ther)/', "Æ", $name );
	$name = preg_replace('/ \(\d\)| \/\/.*/','', $name);

	// Exceptions
	if ( strcmp('_____', $name) == 0 ) {
		$cards[] = $name;
	}
	else if ( strcmp('Who/What/When/Where/Why', $name) == 0 ) {
		$cards[] = preg_replace("/\/(?=w)/", " ", strtolower($name));
	}
	else if ( strcmp("B.F.M.", $name) == 0 ) {
			$cards[] = "b.f.m. 1";
			$cards[] = "b.f.m. 2";
	}
	else if ( strcmp("Kill Destroy", $name) == 0 ) {
		$cards[] = "kill! destroy!";
	}
	else if ( preg_match("/The Ultimate Nightmare of Wizards/", $name) ) {
		$cards[] = "the ultimate nightmare of wizards of the coastr customer service";
	}
	else if ( strcmp("Naughty/Nice", $name) == 0 ) {
		$cards[] = "naughtynice";
	}
	else {

		$params = array( ':name' => $name, ':set' => $set );

		$query = $db->prepare('
			SELECT `imageName` FROM `cards` WHERE `name` LIKE :name AND `set` = :set
		');

		$query->execute($params);

		$result = $query->fetchAll(PDO::FETCH_ASSOC);

		$cards = array();

		foreach ($result as $item) {
			//var_dump($item);
			$cards[] = $item['imageName'];
		}
	}
	
		return $cards;


	}


	function isUpdating($db) {
		$check = $db->prepare("
			SELECT isUpdating FROM metadata LIMIT 1
		");

		if ( $check->execute() ) {
			$result = $check->fetch();

			if ( $result[0] == 0 ) {
				return false;
			}
			else {
				return true;
			}
		}
		else {
			die("Error\n");
		}	
	}

	function setUpdating($db, $setting) {
		$params = array( ':setting' => $setting );

		$change = $db->prepare("
			UPDATE metadata SET isUpdating = :setting 
		");

		$change->execute($params);
	}
?>
