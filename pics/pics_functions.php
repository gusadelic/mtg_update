<?php
include ('../mtg_functions.inc.php');


function getIcons($doc) {

	$finder = new DomXPath($doc);
	$classname = "icon";
	$nodes = $finder->query("//*[contains(@class, '$classname')]"); 
	$icons = array();
	foreach ( $nodes as $node ) {
        	$imgs = $node->getElementsByTagName('img');
		for ( $i = 0 ; $i < $imgs->length ; $i++ ) {
			$img = $imgs->item($i);
			if ($img === null) {
 			  echo("The image was not found!\n");
			}
			else {
				$icons[] = $img->getAttribute('src');
			}
		}
	}
	return $icons; 		
}




function printElements($node) {

    for ($i = 0; $i < $node->length; $i++) {
        echo $node->item($i)->getNodePath() . "\n";
    }

}

function getLogos($doc) {

	$finder = new DomXPath($doc);
	$classname = "logo";
	$nodes = $finder->query("//*[contains(@class, '$classname')]"); 
	$logos = array();
	foreach ( $nodes as $node ) {
        	$imgs = $node->getElementsByTagName('img');
		for ( $i = 0 ; $i < $imgs->length ; $i++ ) {
			$img = $imgs->item($i);
			if ($img === null) {
 			  echo("The image was not found!\n");
			}
			else {
				$logos[] = $img->getAttribute('src');
			}
		}
	}
	return $logos; 		
}



function getSymbols() {
	$icons = array();
	$logos = array();
	
        $html = file_get_contents_curl("http://magic.wizards.com/en/game-info/products/card-set-archive");
	$doc = new DOMDocument();
	@$doc->loadHTML($html);
	       
	$icons = getIcons($doc);
	$logos = getLogos($doc);

	$symbols = array( "icons" => $icons, "logos" => $logos );

	return $symbols;
}

?>
