<?php
require ('pics_functions.php');
require ('../mtg_global.inc.php');

function insertSet($code, $icon, $logo, $db) {
	$params = array(
		':code' => $code,
		':icon' => $icon,
		':logo' => $logo
	);

	$insert = $db->prepare("
		INSERT INTO set_images (code, icon, logo) VALUES (:code, :icon, :logo)
	");
	
	$insert->execute($params);

}



$imgs = getSymbols();
$codes = getSetCodes($db);	

$logo = "";
$icon = "";

foreach ($codes as $code) {
	foreach ($imgs['logos'] as $img) {
		if ( preg_match("/" . $code->code . "/",  $img) == 1 ) {
			$logo = $img;
		}
	}
	foreach ($imgs['icons'] as $img) {
		if ( preg_match("/" . $code->code . "/",  $img) == 1 ) {
			$icon = $img;
		}
	}

	//echo $code->code . ": " . $logo . " - " . $icon . "\n";
	insertSet($code->code, $icon, $logo, $db);
	$logo = "";
	$icon = "";
}



?>
